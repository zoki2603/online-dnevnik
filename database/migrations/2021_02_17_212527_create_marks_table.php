<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   Schema::enableForeignKeyConstraints();
        Schema::create('marks', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id()->unsigned();
            $table->integer('mark');
            $table->string('method');
            $table->string('coment');
            $table->bigInteger('id_student')->unsigned();
            $table->bigInteger('id_profesor')->unsigned();
            $table->bigInteger('id_subject')->unsigned();
            $table->timestamps();
            $table->foreign('id_student')->references('id')->on('students');
            $table->foreign('id_profesor')->references('id')->on('profesors');
            $table->foreign('id_subject')->references('id')->on('subjects');
            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marks');
    }
}
