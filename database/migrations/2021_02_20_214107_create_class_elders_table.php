<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassEldersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_elders', function (Blueprint $table) {
            $table->id()->unsigned();
            $table->bigInteger('id_profesor')->unsigned();
            $table->bigInteger('id_clas')->unsigned();
            $table->timestamps();
            $table->foreign('id_clas')->references('id')->on('clas');
            $table->foreign('id_profesor')->references('id')->on('profesors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class_elders');
    }
}
