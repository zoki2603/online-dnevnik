<?php

namespace App\Models;

use App\Models\Student;
use App\Models\Profesor;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Mark extends Model
{
    use HasFactory;
    protected $tabele = 'Mark';

    public function students()
    {
        return $this->belongsToMany(Student::class);
    }
    public function profesors()
    {
        return $this->hasMany(Profesor::class, 'id', 'id_profesor');
    }
    public function subject()
    {
        return $this->hasMany(Subject::class, 'id','id_subject');
    }
}
