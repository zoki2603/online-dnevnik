<?php

namespace App\Models;

use App\Models\Mark;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Student extends Model
{
    use HasFactory;
    public function marks()
    {
        return $this->belongsToMany(Mark::class);
    }
}
