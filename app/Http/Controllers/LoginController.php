<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
   public function login()
   {
       return view('login.login');
   }
   public function loginUser(Request $request)
   {
       $this->validate($request,[
           'email'=>'required|email',
           'password'=>'required',
           'id_student'=>'required|max:1000'
       ]);

      if (!Auth::attempt($request->only('email','password','id_student'))) {
          return back()->with('status','Your login data is incorect!');
      }

      $userId = Auth::user()->id;
      return redirect('student/'.$userId);
   }


}
