<?php

namespace App\Http\Controllers;

use App\Models\Mark;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MarkController extends Controller
{
    public function marks(Mark $mark,Student $student, $id)
    {
       $student_mark = Mark::where('id_student', $id)->with('profesors','subject')->get();
    //    dd($student_mark);
       $student = Student::where('id', $id)->get();

       return view('student.marks',['student_mark'=>$student_mark,'student'=>$student[0]]);
      
    }

}
