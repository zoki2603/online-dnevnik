<?php

namespace App\Http\Controllers;

use App\Models\Mark;
use App\Models\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function index()
    {
        $students = Student::all();
      
        return view('student.index',['students'=>$students]);
    }

    public function studentInfo($id)
    {
        $student_mark = Mark::where('id_student', $id)->get();
        //    dd($student_mark);
           $student = Student::where('id', $id)->get();
      
        return view('student.student',['student_mark'=>$student_mark,'student'=>$student[0]]);
    }
}
