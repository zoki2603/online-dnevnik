@section('title')
Ocene
    
@endsection
@extends('studentMaster')
@section('contentStudent')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form action="">
                    <table class="table">
                        <thead>
                            <th>Ime Profesora</th>
                            <th>Ocena</th>
                            <th>Ime predmeta</th>
                            <th>Komentar</th>
                        </thead>
                        <tbody>
                           @foreach ($student_mark as $mark)
                               <tr>
                                   <td>{{ $student_mark[0]->profesors[0]->name }}</td>
                                   <td>{{ $mark->mark }}</td>
                                   <td>{{ $student_mark[0]->subject[0]->name_of_subject }}</td>
                                   <td>{{ $mark->coment }}</td>

            
                               </tr>
                           @endforeach
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>
@endsection