@section('title')
Ocene
    
@endsection
@extends('studentMaster')
@section('contentStudent')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form action="">
                    <table class="table">
                        <thead>
                            <th>Redni broj</th>
                            <th>Ime studenta</th>
                        </thead>
                        <tbody>
                           @foreach ($students as $student)
                               <tr>
                                    <td>{{ $student->id }}</td>
                                    <td><a href="{{ route('student', $student->id) }}">{{ $student->name }}</a></td>
                               </tr>
                           @endforeach
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>
@endsection