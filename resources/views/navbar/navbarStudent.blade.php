

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    
    <a class="btn btn-light" data-bs-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">Student</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="nav justify-content-end " id="navbarNavDropdown">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="#">Raspored casova</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('marks', $student['id']) }}">Ocene</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Izostanci</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Najava testa</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Kontakt</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
           Action
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <li><a class="dropdown-item" href="#">Action</a></li>
            <li><a class="dropdown-item" href="{{ route('logout') }}">Logout</a></li>
            <li><a class="dropdown-item" href="#">Something else here</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>
<div class="row">
  <div class="col-md-3">
      <div class="collapse multi-collapse" id="multiCollapseExample1">
        <div class="card">
        <div class="card-header">
            <h4 class="text-center">{{ $user->name }}</h4>
        </div>
           <div class="card card-body">
              <img src="{{ asset('/images/supermen.jpg') }}" class="rounded-circle" alt="Slika ucenika">
          </div>
        </div>
        </div>
      </div>
   </div>
  
      