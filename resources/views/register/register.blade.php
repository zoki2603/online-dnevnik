@section('title')
    Register
@endsection
@extends('master')
@section('content')

<div class="container pt-5">
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="card">
                <div class="card-header">
                   <h3>Register</h3>
                </div>
                @if (session('status'))
                <div class="alert alert-danger" role="alert">
                   {{ session('status') }}
                  </div>
                    
                @endif
                <div class="card-body">
                    <form action="{{ route('register') }}" method="POST" >
                        @csrf
                        <div class="mb-3">
                            <label for="name" class="form-label">Name</label>
                            <input type="text" class="form-control" name="name" id="name" >
                          </div>
                          <div class="mb-3">
                            <label for="email" class="form-label">Email address</label>
                            <input type="email" class="form-control" name="email" id="email" >
                          </div>
                        <div class="mb-3">
                          <label for="password" class="form-label">Password</label>
                          <input type="password" name="password" class="form-control" id="password">
                        </div>
                        <div class="mb-3">
                            <label for="password_confirmation" class="form-label">Confirm Password</label>
                            <input type="password" name="password_confirmation" class="form-control" id="password_confirmation">
                          </div>
                        <div class="d-grid gap-2">
                            <button type="submit" class="btn btn-success">Login</button>
                        </div>
                      </form> 
                </div>
            </div>
        </div>
    </div>
</div>
    
@endsection